<?php
/**
 * Transliterate plugin for Craft CMS 3.x
 *
 * Transliterate characters from one script to another
 *
 * @link      https://grom.digital
 * @copyright Copyright (c) 2020 Marko Raspor
 */

namespace gromdigital\transliterate\twigextensions;

use Craft;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators,
 * global variables, and functions. You can even extend the parser itself with
 * node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 *
 * @author    Marko Raspor
 * @package   Transliterate
 * @since     1.0.0
 */
class TransliterateTwigExtension extends AbstractExtension
{
    // Public Methods
    // =========================================================================

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName(): string
    {
        return 'Transliterate';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return [
            new TwigFilter('transliterate', [$this, 'transliterateString']),
        ];
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('transliterate', [$this, 'transliterateString']),
        ];
    }

    /**
     * Our function called via Twig; it can do anything you want
     *
     * @param null            $text
     * @param int|string|null $toScript
     *
     * @return string
     */
    public function transliterateString($text = null, $toScript = null): ?string
    {
        if (null !== $text) {

            $transliteratorId = null;

            if (!is_numeric($toScript)) {

                if ("serbian_latin2cyrillic_script" === $toScript) {

                    if (false === $this->isHTML($text)) {
                        return $this->replaceSerbianChars($text);
                    }

                    $dom = new \DomDocument();
                    $dom->loadHTML($text, LIBXML_HTML_NOIMPLIED |
                        LIBXML_HTML_NODEFDTD |
                        LIBXML_NOERROR |
                        LIBXML_NOWARNING |
                        LIBXML_PARSEHUGE |
                        LIBXML_BIGLINES |
                        LIBXML_COMPACT);
                    $xpath = new \DOMXPath($dom);

                    foreach ($xpath->query('//text()') as $content) {
                        if (trim($content->nodeValue)) {
                            $content->nodeValue = $this->replaceSerbianChars(utf8_decode($content->nodeValue));
                        }
                    }

                    return $dom->saveHTML();
                }

                $transliteratorId = $toScript;
            }

            if (is_numeric($toScript)) {
                $transliteratorId = transliterator_list_ids()[$toScript];
            }

            $transliterator = \Transliterator::create($transliteratorId);

            $text = null !== $transliterator ? $transliterator->transliterate($text) : '';
        }

        return $text;
    }

    /**
     * @param $text
     * @return bool
     */
    private function isHTML($text): bool
    {
        return preg_match("/<[^<]+>/", $text, $m) !== 0;
    }

    /**
     * @param string     $htmlText
     * @param array|null $script
     * @return string
     */
    private function replaceSerbianChars($htmlText, array $script = null): string
    {
        if (null === $script) {
            $script = $this->getSerbianScript();
        }

        //return str_replace($script['spec1'], $script['spec2'], str_replace($script['lat'], $script['cyr'], utf8_decode($htmlText)));
        return str_replace($script['spec1'], $script['spec2'], str_replace($script['lat'], $script['cyr'], $htmlText));
    }

    /**
     * @return string[][]
     */
    private function getSerbianScript(): array
    {
        $lat = [
            "A", "B", "V", "G", "DŽ", "D", "Đ", "E", "Ž", "Z", "I", "J", "K", "LJ", "L",
            "M", "NJ", "N", "O", "P", "R", "S", "T", "Ć", "U", "F", "H", "C", "Č", "Š",
            "a", "b", "v", "g", "dž", "d", "đ", "e", "ž", "z", "i", "j", "k", "lj", "l",
            "m", "nj", "n", "o", "p", "r", "s", "t", "ć", "u", "f", "h", "c", "č", "š",
        ];

        $cyr = [
            "A", "Б", "В", "Г", "Џ", "Д", "Ђ", "Е", "Ж", "З", "И", "Ј", "К", "Љ", "Л",
            "М", "Њ", "Н", "О", "П", "Р", "С", "Т", "Ћ", "У", "Ф", "Х", "Ц", "Ч", "Ш",
            "a", "б", "в", "г", "џ", "д", "ђ", "е", "ж", "з", "и", "ј", "к", "љ", "л",
            "м", "њ", "н", "о", "п", "р", "с", "т", "ћ", "у", "ф", "х", "ц", "ч", "ш",
        ];

        $spec1 = [
            'НJ', 'Нј', 'нј', 'ЛJ', 'Лј', 'лј', 'ДЖ', 'Дж', 'дж',
        ];

        $spec2 = [
            'Њ', 'Њ', 'њ', 'Љ', 'Љ', 'љ', 'Џ', 'Џ', 'џ',
        ];

        return ["lat" => $lat, "cyr" => $cyr, "spec1" => $spec1, "spec2" => $spec2];
    }
}
