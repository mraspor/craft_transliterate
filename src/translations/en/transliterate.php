<?php
/**
 * Transliterate plugin for Craft CMS 3.x
 *
 * Transliterate characters from one script to another
 *
 * @link      https://grom.digital
 * @copyright Copyright (c) 2020 Marko Raspor
 */

/**
 * Transliterate en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('transliterate', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Marko Raspor
 * @package   Transliterate
 * @since     1.0.0
 */
return [
    'Transliterate plugin loaded' => 'Transliterate plugin loaded',
];
